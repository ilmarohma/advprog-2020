package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;


import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    HolyGrail hg;

    @BeforeEach
    public void setUp() throws Exception {
        hg = new HolyGrail();
    }

    @Test
    public void  testMakeAWish(){
        hg.makeAWish("test");
        assertEquals("test", hg.getHolyWish().getWish());
    }
}
