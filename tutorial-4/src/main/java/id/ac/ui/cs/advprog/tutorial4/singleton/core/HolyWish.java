package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import javax.security.auth.kerberos.KerberosTicket;

public class HolyWish {

    private String wish;

    // TODO complete me with any Singleton approach
    private static HolyWish holy;

    private HolyWish(){}
    public static HolyWish getInstance(){
        if(holy == null){
            holy = new HolyWish();
        }
        return holy;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
