package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class LordranArmory implements Armory {

    @Override
    public Armor craftArmor() {
        Armor armor = new ShiningArmor();
        return armor;
    }

    @Override
    public Weapon craftWeapon() {
        Weapon weapon = new ShiningBuster();
        return weapon;
    }

    @Override
    public Skill learnSkill() {
        Skill skill = new ShiningForce();
        return skill;
    }
}
