package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Random rand = new Random();
    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int val = 1 + rand.nextInt(5);
        return this.weapon.getWeaponValue()+val;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return this.weapon.getDescription()+" Regular";
    }
}
