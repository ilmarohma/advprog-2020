package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
    private String childName;
    private  String childRole;
    private ArrayList<Member> lst;

    public OrdinaryMember(String childName, String childRole){
        this.childName = childName;
        this.childRole = childRole;
        this.lst = new ArrayList<Member>();
    }

    @Override
    public String getName() {
        return childName;
    }

    @Override
    public String getRole() {
        return childRole;
    }

    @Override
    public void addChildMember(Member member) {
    }

    @Override
    public void removeChildMember(Member member) {
    }

    @Override
    public List<Member> getChildMembers() {
        return this.lst;
    }
}
