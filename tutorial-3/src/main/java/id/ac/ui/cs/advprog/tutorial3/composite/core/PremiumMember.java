package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    private String childName;
    private  String childRole;
    private ArrayList<Member> lst;

    public PremiumMember(String childName, String childRole){
        this.childName = childName;
        this.childRole = childRole;
        this.lst = new ArrayList<Member>();
    }

    @Override
    public String getName() {
        return childName;
    }

    @Override
    public String getRole() {
        return childRole;
    }

    @Override
    public void addChildMember(Member member) {
        if(this.childRole.equals("Master")) {
            this.lst.add(member);
        }
        else if(this.lst.size() < 3) {
            this.lst.add(member);
        }

    }

    @Override
    public void removeChildMember(Member member) {
        this.lst.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.lst;
    }
}
