package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    Random rand = new Random();

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int val = 5 + rand.nextInt(6);
        return this.weapon.getWeaponValue()+val;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return this.weapon.getDescription()+" Raw";
    }
}
