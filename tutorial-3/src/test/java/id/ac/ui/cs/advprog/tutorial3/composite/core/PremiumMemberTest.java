package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member mem = new PremiumMember("TestName", "TestRole");
        member.addChildMember(mem);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member mem = new PremiumMember("TestName", "TestRole");
        member.addChildMember(mem);
        member.removeChildMember(mem);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member mem = new PremiumMember("TestName", "TestRole");
        member.addChildMember(mem);
        Member mem1 = new PremiumMember("TestName", "TestRole");
        member.addChildMember(mem1);
        Member mem2 = new PremiumMember("TestName", "TestRole");
        member.addChildMember(mem2);
        assertEquals(3, member.getChildMembers().size());
        Member mem3 = new PremiumMember("TestName", "TestRole");
        member.addChildMember(mem3);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
    }
}
