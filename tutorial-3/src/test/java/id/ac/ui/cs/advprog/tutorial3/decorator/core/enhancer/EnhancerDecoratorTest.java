package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1 = new Gun();
    Weapon weapon2 = new Longbow();
    Weapon weapon3 = new Sword();
    Weapon weapon4 = new Shield();
    Weapon weapon5 = new Shield();
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        assertEquals(" Automatic Gun type Chaos", weapon1.getDescription());
        assertEquals("Big Longbow type Magic", weapon2.getDescription());
        assertEquals("Great Sword type Regular", weapon3.getDescription());
        assertEquals("Heater Shield type Raw", weapon4.getDescription());
        assertEquals("Heater Shield type Unique", weapon5.getDescription());


    }

}
