package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member mem = new OrdinaryMember("testName", "testRole");
        guild.addMember(guildMaster, mem);
        assertTrue(guild.getMemberList().size()==2);
    }

    @Test
    public void testMethodRemoveMember() {
        Member mem = new OrdinaryMember("testName", "testRole");
        Member mem1 = new OrdinaryMember("testName1", "testRole1");
        guild.addMember(guildMaster, mem);
        guild.addMember(guildMaster, mem1);
        guild.removeMember(guildMaster, mem1);
        assertTrue(guild.getMemberList().size()==2);

    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
    }
}
